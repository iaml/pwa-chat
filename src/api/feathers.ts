import io from 'socket.io-client';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';

export const socket = io('https://demo-chat-backend.herokuapp.com/', {
    reconnection: true,
});
const client = feathers();

client.configure(socketio(socket));
client.configure(auth({}));

export default client;
