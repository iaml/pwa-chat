import { DefaultTheme } from 'styled-components';

export const THEME_DEFAULT: DefaultTheme = {
    out: {
        background: 'salmon',
        foreground: 'white',
        accent: 'lightBlue',
    },
    in: {
        background: 'white',
        foreground: 'black',
        accent: 'salmon',
    },
};

export const tr: (...args: string[]) => string = (...props) => props.map(prop => `${prop}  200ms ease-out`).join(', ');
