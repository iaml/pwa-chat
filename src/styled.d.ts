// import original module declarations
import 'styled-components';

type ColorSet = {
    background: string;
    foreground: string;
    accent: string;
}

declare module 'styled-components' {
    export interface DefaultTheme {
        in: ColorSet;
        out: ColorSet;
    }
}
