import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router';
import { connect } from 'react-redux';

import Dialogs from './feature/Dialog';
import Chat from './feature/Chat';
import { IRootState } from './store';
import { Login } from './feature/Auth';

export const Router: React.FC<{ isLoggedIn: boolean }> = ({ isLoggedIn }) => {
    if (!isLoggedIn) return <Login />;
    return (
        <BrowserRouter basename="pwa-chat/">
            <Switch>
                <Route path="/" exact component={Dialogs} />
                <Route path="/:chatId" exact component={Chat} />
            </Switch>
        </BrowserRouter>
    );
};

export default connect((state: IRootState) => ({ isLoggedIn: state.auth.token.length !== 0 }))(Router);
