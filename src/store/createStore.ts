import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import rootReducer, { rootSaga } from '.';

export default function configureAppStore() {
    const saga = createSagaMiddleware();
    const preloadedState = localStorage.getItem('state');
    const store = configureStore({
        reducer: rootReducer,
        middleware: [...getDefaultMiddleware({ thunk: false }), saga],
        preloadedState: preloadedState === null ? undefined : JSON.parse(preloadedState),
    });
    saga.run(rootSaga);
    store.subscribe(() => localStorage.setItem('state', JSON.stringify(store.getState())));

    // if (process.env.NODE_ENV !== 'production' && module.hot) {
    //     module.hot.accept('.', () => {
    //         const newRootReducer = require('.').default;
    //         store.replaceReducer(newRootReducer);
    //     });
    // }

    return store;
}
