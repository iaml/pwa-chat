import { channel } from 'redux-saga';
import { combineReducers, createAction } from '@reduxjs/toolkit';
import { all, put, takeEvery, spawn, call } from '@redux-saga/core/effects';

import auth from '../feature/Auth/Auth.slice';
import { authSaga } from '../feature/Auth/Auth.saga';
import chat from '../feature/Chat/Chat.slice';
import { chatSaga } from '../feature/Chat/Chat.saga';
import service from '../feature/Service/Service.slice';
import { serviceSaga } from '../feature/Service/Service.saga';

export { setCurrentUser } from '../feature/Auth/Auth.slice';


export const rootReducer = combineReducers({
    auth,
    chat,
    service,
});

export type IRootState = ReturnType<typeof rootReducer>;

export const actionChannel = channel();
export function* actionPortal(action: any) {
    yield put(action);
}
function* actionSaga() {
  yield takeEvery(actionChannel, actionPortal);
}

export function* rootSaga() {
    const sagas = [
        authSaga,
        chatSaga,
        serviceSaga,
        actionSaga
    ];
  
    yield all(sagas.map(saga =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga)
            break
          } catch (e) {
            console.error(e)
          }
        }
      }))
    );
  }

export const initApp = createAction('app/init');

export default rootReducer;
