import styled, { css, FlattenSimpleInterpolation } from 'styled-components';

const shared: Record<string, FlattenSimpleInterpolation> = {
    top: css`
        position: fixed;
        top: 2ch;
        left: 0;
        right: 0;
        height: 7ch;
        margin-top: -2ch;
        z-index: 1;
    `,
    bottom: css`
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        height: 7ch;
    `,
};

export const Fade = styled.aside.attrs(({ type }: { type: string }) => ({ shared: shared[type] }))<{
    blur?: boolean;
    darken?: boolean;
    type: string;
    shared: FlattenSimpleInterpolation;
}>`
    background: linear-gradient(to ${({type}) => type}, rgba(0, 0, 0, 0) 5%, rgb(0, 0, 0, 1) 75%);
    ${({ blur, shared }) =>
        blur &&
        css`
            &:before {
                content: '';
                display: block;
                backdrop-filter: blur(25px);
                mask-image: linear-gradient(rgba(0, 0, 0, 0), rgb(0, 0, 0, 1) 95%);
                ${shared}
            }
        `}
    ${({ darken, shared }) =>
        darken &&
        css`
            &:after {
                content: '';
                display: block;
                backdrop-filter: brightness(0);
                mask-image: linear-gradient(rgba(0, 0, 0, 0), rgb(0, 0, 0, 1));
                ${shared}
            }
        `}
    ${({ shared }) => shared}
`;
