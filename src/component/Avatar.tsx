import React from 'react';
import styled from 'styled-components';
import { adjustHue } from 'polished';

import { stringToColor } from '../utils/color';

const DEFAULT_AVATAR_SIZE = '4ch';

export const AvatarContainer = styled.div<{ size?: string }>`
    width: ${({ size = DEFAULT_AVATAR_SIZE }) => size};
    max-width: ${({ size = DEFAULT_AVATAR_SIZE }) => size};
    height: ${({ size = DEFAULT_AVATAR_SIZE }) => size};
    border-radius: 50%;
    overflow: hidden;
    margin: 8px;
    display: inline-block;
`;

const Gradient = styled.div<{ from: string; to: string }>`
    background: linear-gradient(
        156.87deg,
        ${({ from }) => from} 14.06%,
        ${({ to }) => to} 80.35%,
        ${({ to }) => to} 80.35%
    );
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    font-size: 17px;
`;

export const Avatar: React.FC<{ img?: string; title: string; flatColor?: boolean; size?: string }> = ({
    img,
    title,
    flatColor = false,
    size,
}) => {
    const userColor = stringToColor(title);
    const initials = title
        .split(' ')
        .map(st => st[0])
        .join('');
    return (
        <AvatarContainer {...{ size }}>
            {img ? (
                <img src={img} alt={`${title}'s avatar`} />
            ) : (
                <Gradient from={userColor} to={flatColor ? userColor : adjustHue(90, userColor)}>
                    {initials}
                </Gradient>
            )}
        </AvatarContainer>
    );
};

export default React.memo(Avatar);
