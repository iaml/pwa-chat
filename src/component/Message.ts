import styled, { css } from 'styled-components';
import { IMessage } from '../models';

export const Message = styled.div<{ direction?: 'in' | 'out', status?: IMessage["status"] }>`
    background-color: ${({ direction, theme }) => theme[direction || 'in'].background};
    color: ${({ direction, theme }) => theme[direction || 'in'].foreground};
    border-radius: 0.7ch;
    padding: 0.5ch 1.2ch 0.6ch;
    position: relative;
    margin-bottom: 2.6ch;
    hyphens: auto;
    line-height: 1.4em;
    max-width: 84vw;
    font-size: 16px;
    align-self: ${({ direction }) => (direction === 'in' ? 'flex-start' : 'flex-end')};
    opacity: ${({status}) => status === 'waiting' ? 0.7 : 1};
    cursor: pointer;
    header {
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        color: ${({ direction, theme }) => theme[direction || 'in'].accent};
        line-height: 2.2ch;
        margin-bottom: 0.4ch;
    }
    ${({ direction }) =>
        direction &&
        css`
            &:before {
                content: '';
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 30px 30px 0;
                border-color: transparent ${({ theme }) => theme[direction || 'in'].background} transparent transparent;
                position: absolute;
                z-index: -1;
                ${direction === 'in' &&
                    css`
                        transform: translate(-40%, -50%) rotate(180deg);
                    `}
                ${direction === 'out' &&
                    css`
                        right: 0;
                        bottom: 0;
                        transform: translate(0%, 30%);
                    `}
            }
        `}
`;
