import React from 'react';
import styled from 'styled-components';
import { Fade } from './Fade';
import { Link } from 'react-router-dom';

const ContentBar = styled.nav<{ type: 'bottom' | 'top' }>`
    position: fixed;
    ${({ type }) => type}: 0;
    left: 0;
    right: 0;
    z-index: 2;
    display: flex;
    justify-content: space-between;
    ${({ type }) => type === 'bottom' && 'margin-bottom: 2ch;'}
`;

export const BarLink = styled(Link)`
    padding: 1ch;
    font-size: 1.2rem;
    color: ${({ theme }) => theme.out.background};
    text-decoration: none;
    span {
        font-size: 1.2rem;
    }
`;
export const BarButton = styled.button`
    padding: 1ch;
    font-size: 1.2rem;
    color: ${({ theme }) => theme.out.background};
    text-decoration: none;
    background: none;
    border: none;
    span {
        font-size: 1.2rem;
    }
`;

export const AppBar: React.FC<{ type?: 'bottom' | 'top' }> = ({ children, type = 'top' }) => {
    return (
        <>
            <Fade {...{ type }} />
            <ContentBar {...{ type }}>{children}</ContentBar>
        </>
    );
};
