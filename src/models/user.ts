export type IUser = {
    firstName?: string;
    lastName?: string;
    email: string;
    id: number;
    subscription?: string;
}

export type ICredentials = {
    email: string;
    password: string;
}