
export type IDialog = {
    id: number;
    user1: string;
    user2: string;
};

export type IMessage = {
    id?: number;
    text: string;
    senderId: number;
    receiverId: number;
    status: 'waiting' | 'pending' | 'sent' | 'seen' | 'error';
    ts: number;
    uuid: string;
};

export type IDirectedMessage = {
    direction: 'in' | 'out';
} & IMessage;