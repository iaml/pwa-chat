import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { THEME_DEFAULT } from './assets/theme';
import createStore from './store/createStore';
import Router from './Router';

const store = createStore();

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <ThemeProvider theme={THEME_DEFAULT}>
                <div className="App">
                    <Router />
                </div>
            </ThemeProvider>
        </Provider>
    );
};

export default App;
