import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { IUser } from '../../models/user';
import { Avatar } from '../../component/Avatar';
import { DialogList, Thread, InvitationBlock } from './Dialog.style';
import { Message } from '../../component/Message';
import { Fade } from '../../component/Fade';
import { AppBar, BarButton } from '../../component/AppBar';
import { getUsers, setToken } from '../Auth/Auth.slice';
import { installApp } from '../Service/Service.slice';
import { selectDialogs } from '../Chat/Chat.slice';

import {ConnectProps} from '.';

export const Dialog: React.FC<{
    dialogs: ReturnType<typeof selectDialogs>;
    user?: IUser;
    getUsers: typeof getUsers;
    setToken: typeof setToken;
} & ConnectProps> = ({ canInstall ,dialogs, user, getUsers, setToken }) => {
    useEffect(() => {
        getUsers();
    }, [getUsers]);
    const dispatch = useDispatch();
    return (
        <DialogList>
            {user && (
                <AppBar type="top">
                    <BarButton>{user.email}</BarButton>
                    {canInstall ? <BarButton onClick={() => dispatch(installApp())}>Install</BarButton> : null}
                    <BarButton onClick={() => setToken('')}>Logout</BarButton>
                </AppBar>
            )}
            {dialogs.map((item) => (
                <Thread to={`/${item.id}`} key={item.id}>
                    <Avatar title={item.email} />
                    {item.lastMessage ? (
                        <Message>
                            <header>{item.email}</header>
                            {item.lastMessage.text}
                        </Message>
                    ) : (
                        <InvitationBlock>Start chatting with {item.email} 👋</InvitationBlock>
                    )}
                </Thread>
            ))}
            <Fade type="bottom" />
        </DialogList>
    );
};
