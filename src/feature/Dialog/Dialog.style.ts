import styled from 'styled-components';
import { AvatarContainer } from '../../component/Avatar';
import { Link } from 'react-router-dom';
import { Message } from '../../component/Message';

export const DialogList = styled.main`
    margin: 7ch 1.2ch 5ch;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const InvitationBlock = styled.div`
    color: white;
    min-width: 40vw;
    text-align: center;
    border: 1px solid white;
    border-radius: 1ch;
    padding: 1ch 2.4ch 1.2ch;
`;

export const Thread = styled(Link)`
    display: flex;
    align-items: center;
    justify-content: center;
    text-decoration: none;
    color: initial;
    transition: ease-in 200ms;
    max-width: 84vw;
    transform: scale(0.95);

    &:active,
    &:hover,
    :focus {
        transform: scale(1);
    }

    ${AvatarContainer} {
        margin-right: 1ch;
        flex: 1 0 4ch;
    }
    ${InvitationBlock}, ${Message} {
        margin-bottom: 1.2ch;
        min-width: 72vw;
        box-sizing: border-box;
    }
`;
