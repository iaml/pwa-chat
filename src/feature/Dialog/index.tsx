import { connect } from 'react-redux';

import { IRootState } from '../../store';
import { getUsers, setToken } from '../Auth/Auth.slice';
import { selectDialogs } from '../Chat/Chat.slice';
import { Dialog } from './Dialog';

export type ConnectProps = ReturnType<typeof mapState>;

const mapState = (state: IRootState) => ({
    canInstall: state.service.shouldDisplayInstallPrompt,
    user: state.auth.currentUser,
    dialogs: selectDialogs(state),
});

export default connect(mapState, { getUsers, setToken })(Dialog);
