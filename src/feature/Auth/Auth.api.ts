import client from '../../api/feathers';
import { ICredentials, IUser } from '../../models/user';

export const apiUsers = client.service('users');

export const apiGetProfile = () => client.get('authentication');
export const apiGetUsers = () => apiUsers.find().then((res: { data: object }) => res.data);

export const apiLogin = ({ email, password }: ICredentials) =>
    client.authenticate({ strategy: 'local', email, password });
export const apiAuthenticate = (accessToken: string) => client.authenticate({ strategy: 'jwt', accessToken });

export const apiSignUp = ({ email, password }: ICredentials) => apiUsers.create({ email, password });

export const apiAddSubscription = (id: number, user: IUser) => apiUsers.update(id, user);