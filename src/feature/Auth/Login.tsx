import React, { useState } from 'react';
import { connect } from 'react-redux';

import { Overlay, Container, LoginButton, Input, Buttons } from './Login.style';
import { signIn, signUp } from './Auth.slice';

export const Login: React.FC<{ signIn: typeof signIn; signUp: typeof signUp }> = ({ signIn, signUp }) => {
    const [login, setEmail] = useState('');
    const makeHandler = (fn: (a1: Record<string, string>) => void) => () => {
        if (login.length < 1) return;
        fn({ email: login, password: login });
    };
    return (
        <Overlay>
            <Container>
                <Input
                    type="text"
                    value={login}
                    placeholder="Username"
                    onChange={({ currentTarget: { value } }: React.FormEvent<HTMLInputElement>) => void setEmail(value)}
                />
                <Buttons>
                    <LoginButton onClick={makeHandler(signIn)}>Log in</LoginButton>
                    <LoginButton onClick={makeHandler(signUp)}>Sign up</LoginButton>
                </Buttons>
            </Container>
        </Overlay>
    );
};

export default connect(null, { signIn, signUp })(Login);
