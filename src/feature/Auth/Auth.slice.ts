import { createSlice, PayloadAction, createSelector, createAction } from '@reduxjs/toolkit';
import { IUser } from '../../models/user';

type IUserState = {
    currentUser?: IUser;
    items: number[];
    asDict: Record<number, IUser>;
    token: string;
};

const initialState: IUserState = {
    currentUser: undefined,
    items: [],
    asDict: {},
    token: '',
};

const slice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setToken(state, action: PayloadAction<string>) {
            state.token = action.payload;
        },
        setCurrentUser(state, action: PayloadAction<IUser | undefined>) {
            state.currentUser = action.payload;
        },
        appendUser(state, { payload: user }: PayloadAction<IUser>) {
            return {
                ...state,
                asDict: {
                    ...state.asDict,
                    [user.id]: user,
                },
                items: [...state.items, user.id],
            };
        },
        setUsers(state, { payload }: PayloadAction<IUser[]>) {
            state.items = payload.map(item => item.id);
            state.asDict = payload.reduce((acc, item) => {
                acc[item.id] = item;
                return acc;
            }, {} as Record<number, IUser>);
        },
    },
});

export default slice.reducer;

export const { appendUser, setToken, setCurrentUser, setUsers } = slice.actions;
export const getUsers = createAction('auth/getUsers');
export const signIn = createAction('auth/signIn', ({ email, password }: Record<string, string>) => ({
    payload: { email, password },
}));
export const signUp = createAction('auth/signUp', ({ email, password }: Record<string, string>) => ({
    payload: { email, password },
}));

export const selectUser = createSelector([], () => null);

