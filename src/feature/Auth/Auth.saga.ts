import { takeEvery, call, put, fork, select } from '@redux-saga/core/effects';

import { signIn, signUp, setToken, setCurrentUser, appendUser, setUsers, getUsers } from './Auth.slice';
import { apiLogin, apiGetProfile, apiSignUp, apiAuthenticate, apiUsers, apiGetUsers, apiAddSubscription } from './Auth.api';
import { IRootState, initApp, actionChannel } from '../../store';
import { IUser } from '../../models/user';
import { registerPush } from '../Service/Service.slice';

function* sagaInit() {
    try {
        const token = yield select((state: IRootState) => state.auth.token);
        apiUsers.on('created', (user: IUser) => {
            actionChannel.put(appendUser(user));
        });
        if (token.length > 0) {
            yield call(apiAuthenticate, token);
            actionChannel.put(initApp());
        }
    } catch (e) {
        console.error(e);
    }
}

function* sagaLogin({ payload: { email, password } }: ReturnType<typeof signIn>) {
    try {
        const { accessToken }: { accessToken: string } = yield call(apiLogin, {
            email,
            password,
        });
        const { user } = yield call(apiGetProfile);
        yield put(setCurrentUser(user));
        yield put(setToken(accessToken));
        actionChannel.put(initApp());
    } catch (e) {
        console.error(e);
    }
}

function* sagaSignUp({ payload: { email, password } }: ReturnType<typeof signUp>) {
    try {
        const user = yield call(apiSignUp, { email, password });
        yield put(setCurrentUser(user));
        yield put(signIn({ email, password }))
    } catch (e) {
        console.error(e);
    }
}

function* sagaGetUsers() {
    try {
        const users = yield call(apiGetUsers);
        yield put(setUsers(users));
    } catch (e) {
        console.error(e);
    }
}

function* sagaWriteSubscription({payload}: ReturnType<typeof registerPush>) {
    const user = yield select((state: IRootState) => state.auth.currentUser);
    yield call(apiAddSubscription, user.id, {...user, subscription: payload});
}

function* sagaClearLS({payload}: ReturnType<typeof setToken>) {
    if (payload.length === 0) yield call(() => localStorage.clear());
}

export function* authSaga() {
    yield fork(sagaInit);
    yield takeEvery(signIn, sagaLogin);
    yield takeEvery(signUp, sagaSignUp);
    yield takeEvery(getUsers, sagaGetUsers);
    yield takeEvery(registerPush, sagaWriteSubscription);
    yield takeEvery(setToken, sagaClearLS);
}
