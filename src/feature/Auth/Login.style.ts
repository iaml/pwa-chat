import styled from 'styled-components';
import { tr } from '../../assets/theme';

export const Overlay = styled.main`
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    height: 100vh;
`;

export const Container = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

export const UserItem = styled.div<{ selected: boolean }>`
    background-color: ${({ selected }) => (selected ? 'white' : 'black')};
    color: ${({ selected }) => (selected ? 'black' : 'white')};
    transition: ${tr('background-color', 'color')};
    padding: 3ch 5ch;
    user-select: none;
    cursor: pointer;
`;

export const LoginButton = styled.button`
    padding: 0.6ch;
    flex: 1;
    background: none;
    border: none;
    color: white;
    font-size: 1rem;
    cursor: pointer;
    transition: ${tr('background-color')};
    &:active,
    &:focus,
    &:hover {
        background-color: rgba(255, 255, 255, 0.4);
    }
`;

export const Input = styled.input`
    background: black;
    color: white;
    font-size: 16px;
    border: none;
    margin: 0.6ch 0.2ch;
    padding: 1ch;
    transition: ${tr('background-color')};
    text-align: center;
    &:active,
    &:focus {
        background-color: ${({
                theme: {
                    out: { background },
                },
            }) => background};
    }
`;

export const Buttons = styled.div`
    display: flex;
    flex-direction: column;
`;
