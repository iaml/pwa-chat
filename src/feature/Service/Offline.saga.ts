import { put, takeLatest } from '@redux-saga/core/effects';

import { actionChannel, initApp } from '../../store';
import { setOfflineStatus } from './Service.slice';
import { socket } from '../../api/feathers';

const setOfflineHandler = (isOffline: boolean) => () => actionChannel.put(setOfflineStatus(isOffline));

function* getInitialState() {
    const isOnline = navigator.onLine;
    if (isOnline) yield put(setOfflineStatus(false));
    window.addEventListener('online', setOfflineHandler(false));
    window.addEventListener('offline', setOfflineHandler(true));
    socket.on('disconnect', setOfflineHandler(false));
    socket.on('connect', setOfflineHandler(true));
}

export function* offlineSaga() {
    yield takeLatest(initApp, getInitialState);
}
