import { call, fork, take, takeEvery, takeLatest } from '@redux-saga/core/effects';

import { actionChannel, initApp } from '../../store';
import { showPrompt, installApp, registerPush, resolveRegistration } from './Service.slice';
import { BeforeInstallPromptEvent } from './';
import * as serviceWorker from '../../serviceWorker';
import { urlBase64ToUint8Array } from '../../utils/push';
import { channel } from 'redux-saga';
import { setToken } from '../Auth/Auth.slice';
import { offlineSaga } from './Offline.saga';

let deferredPrompt: BeforeInstallPromptEvent | undefined;
let registration: ServiceWorkerRegistration; 
let subscription: PushSubscription;

const regChannel = channel();

const getSub = async () => {
    const existingSub = await registration.pushManager.getSubscription();
    if (existingSub) return existingSub;
    return registration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(process.env.REACT_APP_VAPID),
    });
}
const unsub = () => {
    if (subscription) subscription.unsubscribe();
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
(process.env.NODE_ENV === 'production' ? serviceWorker.register : serviceWorker.unregister)({
    onSuccess: async reg => {
        registration = reg;
        regChannel.put(resolveRegistration())
    },
});


function suggestInstall() {
    window.addEventListener('beforeinstallprompt', e => {
        deferredPrompt = e as BeforeInstallPromptEvent;
        actionChannel.put(showPrompt(true));
        deferredPrompt.userChoice
          .then(() => {
              actionChannel.put(showPrompt(false))
          });
    });
}

function* sagaInstallApp() {
    if (deferredPrompt === undefined) return;
    try {
        yield call(deferredPrompt.prompt);
    } catch (e) {
        console.error(e);
    }
}

function* sagaSubscribeToPush() {
    yield take(regChannel);
    subscription = yield call(getSub);
    actionChannel.put(registerPush(JSON.stringify(subscription)));
}

function* sagaUnsubscribe({payload}: ReturnType<typeof setToken>) {
    if (payload.length === 0) yield call(unsub);
}

export function* serviceSaga() {
    yield fork(suggestInstall);
    yield fork(offlineSaga);
    yield takeEvery(installApp, sagaInstallApp);
    yield takeLatest(initApp, sagaSubscribeToPush);
    yield takeEvery(setToken, sagaUnsubscribe)
}
