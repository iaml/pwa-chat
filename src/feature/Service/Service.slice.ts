import { createSlice, PayloadAction, createAction } from '@reduxjs/toolkit';
type IState = {
    shouldDisplayInstallPrompt: boolean;
    isOffline: boolean;
};

const initialState: IState = {
    shouldDisplayInstallPrompt: false,
    isOffline: true,
};

const slice = createSlice({
    name: 'service',
    initialState,
    reducers: {
        setOfflineStatus(state, { payload }: PayloadAction<boolean>) {
            state.isOffline = payload;
        },
        showPrompt(state, { payload }: PayloadAction<boolean | undefined>) {
            state.shouldDisplayInstallPrompt =
                typeof payload === 'boolean' ? payload : !state.shouldDisplayInstallPrompt;
        },
    },
});

export default slice.reducer;

export const { setOfflineStatus, showPrompt } = slice.actions;
export const installApp = createAction('app/install');
export const registerPush = createAction('app/registerPushSub', (subscription: string) => ({ payload: subscription }));
export const resolveRegistration = createAction('app/resolveRegistration');
