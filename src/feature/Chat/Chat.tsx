import React, { useState, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { AppBar, BarLink } from '../../component/AppBar';
import { Message } from '../../component/Message';
import { ChatLayout, History, Field, ChatCtrls, NoHistory } from './Chat.style';
import { useScrollToTop } from '../../utils/hooks';
import { IDirectedMessage } from '../../models';
import { createMessage } from './Chat.slice';

type IChatProps = {
    messages: IDirectedMessage[];
    createMessage: typeof createMessage;
};

export const Chat: React.FC<IChatProps & RouteComponentProps<{ chatId: string }>> = ({
    createMessage,
    match: {
        params: { chatId },
    },
    messages,
}) => {
    const [text, setText] = useState('');
    const sendMessage = (e: FormEvent) => {
        e.preventDefault();
        setText('');
        createMessage({ receiverId: Number(chatId), text });
    };
    const Anchor = useScrollToTop('top');
    return (
        <ChatLayout>
            <AppBar type="top">
                <BarLink to="/">
                    <span>ᗉ</span> Back
                </BarLink>
            </AppBar>
            <History>
                {messages.length === 0 && <NoHistory />}
                {messages.map(({ ts, text, direction, status }) => (
                    <Message key={ts} {...{ direction, status }}>
                        {text}
                    </Message>
                ))}
                <Anchor />
            </History>
            <AppBar type="bottom">
                <ChatCtrls onSubmit={sendMessage}>
                    <Field value={text} onChange={({ currentTarget: { value } }) => setText(value)} />
                    <button onClick={sendMessage}>
                        <span role="img" aria-label="send">
                            💬
                        </span>
                    </button>
                </ChatCtrls>
            </AppBar>
        </ChatLayout>
    );
};
