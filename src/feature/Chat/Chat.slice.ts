import { createSlice, PayloadAction, createSelector, createAction } from '@reduxjs/toolkit';
import { IMessage } from '../../models';
import { IRootState } from '../../store';
import { withDirection } from './Chat.utils';

type IChatState = {
    items: string[];
    asDict: Record<string, IMessage>;
};

const initialState: IChatState = {
    items: [],
    asDict: {},
};

const slice = createSlice({
    name: 'chat',
    initialState,
    reducers: {
        appendMessage(state, { payload: message }: PayloadAction<IMessage>) {
            if (state.asDict.hasOwnProperty(message.uuid)) {
                state.asDict[message.uuid].status = message.status;
            } else {
                state.asDict[message.uuid] = message;
                state.items.push(message.uuid);
            }
        },
        setMessages(state, { payload: messages }: PayloadAction<IMessage[]>) {
            state.items = messages.filter(({uuid}) => state.items.includes(uuid)).map(item => item.uuid);
            state.asDict = messages.reduce((acc, item) => {
                acc[item.uuid] = item;
                return acc;
            }, {} as Record<string, IMessage>);
        },
    },
});

export default slice.reducer;

export const { appendMessage, setMessages } = slice.actions;
export const getMessages = createAction('chat/getMessages');
export const createMessage = createAction(
    'chat/createMessage',
    ({ receiverId, text }: Pick<IMessage, 'receiverId' | 'text'>) => ({
        payload: { receiverId, text },
    }),
);
export const triggerSync = createAction('chat/triggerSync');

export const selectChatId = (_: any, props: { match: { params: { chatId: string } } }) =>
    Number(props.match.params.chatId);
export const selectMessages = ({ chat: { items, asDict } }: IRootState) => items.map(uuid => asDict[uuid]);
export const selectMessagesScoped = createSelector(
    [(state: IRootState) => state.auth.currentUser, selectMessages],
    (currentUser, messages: IMessage[]) => {
        return messages
            .filter(({ senderId, receiverId }) => currentUser!.id === senderId || currentUser!.id === receiverId)
            .map(withDirection(currentUser!));
    },
);
export const selectMessagesToSend = createSelector([selectMessagesScoped], messages =>
    messages.filter(({ status }) => status === 'waiting'),
);
export const selectMessagesTo = createSelector([selectMessagesScoped, selectChatId], (messages, chatId) => {
    return messages.filter(({ senderId, receiverId }) => chatId === senderId || chatId === receiverId);
});
export const selectLastMessages = createSelector([selectMessagesScoped], messages =>
    messages.reduce((acc, item) => {
        acc[item.receiverId] = item;
        acc[item.senderId] = item;
        return acc;
    }, {} as Record<string, IMessage>),
);

export const selectDialogs = createSelector(
    [
        (state: IRootState) => state.auth.items,
        (state: IRootState) => state.auth.asDict,
        (state: IRootState) => state.auth.currentUser,
        selectLastMessages,
    ],
    (ids, dict, currentUser, lastMessages) => {
        return ids
            .filter((id: number) => !(id === currentUser!.id))
            .map((id: number) => {
                const user = dict[id];
                const lastMessage = lastMessages[user.id];
                return {
                    ...user,
                    lastMessage: lastMessage ? withDirection(user)(lastMessage) : undefined,
                };
            });
    },
);
