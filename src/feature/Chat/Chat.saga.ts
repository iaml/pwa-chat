import { race, take, takeEvery, takeLatest, call, put, select, fork } from '@redux-saga/core/effects';
import uuid from 'uuid';

import {
    getMessages,
    setMessages,
    createMessage,
    appendMessage,
    selectMessagesToSend,
    triggerSync,
} from './Chat.slice';
import { apiGetMessages, apiCreateMessage, apiMessages } from './Chat.api';
import { IMessage } from '../../models';
import { IRootState, initApp, actionChannel } from '../../store';
import { setOfflineStatus } from '../Service/Service.slice';

function* sagaInit() {
    yield fork(sagaGetMessages);
    yield fork(sagaSyncMessagesDaemon);

    apiMessages.on('created', (message: IMessage) => {
        actionChannel.put(appendMessage(message));
    });
}

function* sagaGetMessages() {
    const messages = yield call(apiGetMessages);
    yield put(setMessages(messages));
}

function* sagaCreateMessage(action: ReturnType<typeof createMessage>) {
    const currentUser = yield select((state: IRootState) => state.auth.currentUser);
    const message: IMessage = {
        ...action.payload,
        senderId: currentUser.id,
        ts: Date.now(),
        status: 'waiting',
        uuid: uuid(),
    };
    yield put(appendMessage(message));
    yield put(triggerSync());
}

function* sagaDoSync() {
    const messagesToSend: IMessage[] = yield select(selectMessagesToSend);
    for (const message of messagesToSend) {
        const result = yield call(apiCreateMessage, message);
        if (!result) {
            console.error('message sending failed');
            return;
        }
    }
}

function* sagaSyncMessagesDaemon() {
    while (true) {
        const isOnline = yield select((state: IRootState) => !state.service.isOffline);
        if (isOnline) {
            yield race({
                onMsg: yield takeLatest(triggerSync, sagaDoSync),
                statusChange: yield take(setOfflineStatus),
            });
        } else {
            yield take(setOfflineStatus);
        }
    }
}

export function* chatSaga() {
    yield takeEvery(initApp, sagaInit);
    yield takeEvery(getMessages, sagaGetMessages);
    yield takeEvery(createMessage, sagaCreateMessage);
}
