import styled from 'styled-components';

export const ChatLayout = styled.main`
    display: flex;
`;

export const History = styled.div`
    display: flex;
    flex-direction: column;
    margin: 6ch auto;
    width: 90vw;
`;

export const Field = styled.input.attrs({ type: 'text', placeholder: '...' })`
    box-sizing: border-box;
    display: block;
    flex: 1;
`;

export const ChatCtrls = styled.form`
    display: flex;
    align-items: center;
    margin: 0 2ch;
    width: 100%;
    & > * {
        font-size: 16px;
        border-radius: 0.6ch;
        padding: 1ch 1.2ch;
    }
    input {
        border: 2px solid ${({theme}) => theme.out.background};
    }
    button {
        background: ${({theme}) => theme.out.background};
        border: none;
        border-radius: 50%;
        padding: 0;
        width: 36px;
        height: 36px;
        margin-left: 1ch;
    }
`;

export const NoHistory = styled.div.attrs({children: 'No history'})`
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 80vh;
`;