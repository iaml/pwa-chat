import { Chat } from './Chat';
import { connect } from 'react-redux';
import { IRootState } from '../../store';
import { createMessage, selectMessagesTo } from './Chat.slice';

export default connect(
    (state: IRootState, props: React.ComponentProps<typeof Chat>) => ({
        messages: selectMessagesTo(state, props),
    }),
    { createMessage },
)(Chat);
