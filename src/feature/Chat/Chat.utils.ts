import { IMessage, IDirectedMessage } from '../../models';
import { IUser } from '../../models/user';

export const withDirection = (currentUser: IUser) => (item: IMessage): IDirectedMessage => ({
    ...item,
    direction: currentUser!.id === item.receiverId ? 'in' : 'out',
});
