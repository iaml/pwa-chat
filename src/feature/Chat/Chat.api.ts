import client from '../../api/feathers';
import { IUser } from '../../models/user';
import { IMessage } from '../../models';

export const apiMessages = client.service('messages');

export const apiGetMessages = () => apiMessages.find();

export const apiGetMessagesByUser = (user1: IUser, user2: IUser) =>
    apiMessages
        .find({
            query: {
                $or: [
                    { senderId: user1.id, receiverId: user2.id },
                    { senderId: user2.id, receiverId: user1.id },
                ],
            },
        })
        .then((res: { data: object }) => res.data);

export const apiCreateMessage = (message: IMessage) => apiMessages.create(message);
