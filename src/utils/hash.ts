export const hash = (a1: string) => {
    let hash = 0,
        i,
        chr;
    if (a1.length === 0) return hash;
    for (i = 0; i < a1.length; i++) {
        chr = a1.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
