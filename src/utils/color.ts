export function stringToColor(input: string, s = 90, l = 65) {
    let hash = 0;
    for (let i = 0; i < input.length; i++) {
        hash = input.charCodeAt(i) + ((hash << 5) - hash);
    }
    const h = hash % 360;
    return 'hsl(' + Math.abs(h) + ', ' + s + '%, ' + l + '%)';
}
