import React, { useEffect, useRef, Ref } from 'react';

export const useScrollToTop = (to: 'top' | 'bottom' = 'top') => {
    const ref: Ref<HTMLDivElement> = useRef(null);
    const map = {
        'top': 'start',
        'bottom': 'end',
    } as const;
    useEffect(() => {
        ref.current && ref.current.scrollIntoView({ behavior: 'smooth', block: map[to] });
    });
    return () => <div ref={ref}/>;
};
