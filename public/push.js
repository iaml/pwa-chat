/* eslint-disable no-restricted-globals */
console.log('[Service Worker] listening to push');
self.addEventListener('push', event => {
    console.info('[Service Worker] Push Received.');
    const { title, body } = event.data.json();
    event.waitUntil(
        self.registration.showNotification(title, {
            body,
            vibrate: [200, 100, 200],
        }),
    );
});

//   function openPushNotification(event) {
//     console.log("[Service Worker] Notification click Received.", event.notification.data);

//     event.notification.close();
//     event.waitUntil(clients.openWindow(event.notification.data));
//   }
//   self.addEventListener("notificationclick", openPushNotification);
